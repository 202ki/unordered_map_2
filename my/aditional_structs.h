#include<iostream>

template <typename T, typename V>
struct Iterator
{
    T *ptr;
    std::size_t i1 = 0;
    std::size_t i2 = 0;
    Iterator &operator++()
    {
        if (ptr[i1].cp() - i2 <0 )
            i2+=1;
        else
        {
            i2=0;
            i1+=1;
        }
        return *this;
    };
    bool operator!=(Iterator iter)
    {
        return !(i1==iter.i1 && i2==iter.i2 && ptr == iter.ptr);
    };
    V operator*()
    {
        return (*ptr)[i1][i2];
    };
};
template <typename T, typename V>
struct Const_Iterator
{
    T const *ptr;
    std::size_t i1 = 0;
    std::size_t i2 = 0;
    Const_Iterator &operator++()
    {
        if (ptr[i1].cp() - i2 <0 )
            i2+=1;
        else
        {
            i2=0;
            i1+=1;
        }
        return *this;
    };
    bool operator!=(Const_Iterator iter)
    {
        return !(i1==iter.i1 && i2==iter.i2 && ptr == iter.ptr);
    };
    V operator*()
    {
        return (*ptr)[i1][i2];
    };
};