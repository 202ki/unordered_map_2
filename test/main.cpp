#include <my/unordered_map.h>
#include <gtest/gtest.h>

TEST(unordered_map, empty)
{
    unordered_map<int, int> m;
    EXPECT_EQ(true, m.empty());
}
TEST(unordered_map, empty_not_empty)
{
    unordered_map<char, char> m(7);
    EXPECT_EQ(false, m.empty());
}
// TEST(unordered_map, insert_pair_true)
// {
//     unordered_map<char, int> m(2);
//     std::pair<char, int> pair = {'a', 5};
//     m.insert(pair);
//     EXPECT_EQ(1, m.size());
// }
// TEST(unordered_map, insert_pair_false)
// {
//     unordered_map<char, int> m(2);
//     std::pair<char, int> pair1 = {'a', 5};
//     m.insert(pair1);
//     std::pair<char, int> pair2 = {'a', 10};
//     m.insert(pair2);
//     EXPECT_EQ(1, m.size());
// }
// TEST(unordered_map, insert_array_empty)
// {
//     unordered_map<char, int> m;
//     std::pair<char, int> nums[5] = {{'a', 5}, {'b', 4}, {'c', 3}, {'d', 2}, {'e', 1}};
//     using numsElemType = std::remove_all_extents<decltype(&nums[0])>::type;
//     m.insert<numsElemType>(&nums[0], &nums[5]);
//     EXPECT_EQ(true, m.empty());
// }
// TEST(unordered_map, insert_array_size)
// {
//     unordered_map<char, int> m;
//     std::pair<char, int> nums[5] = {{'a', 5}, {'b', 4}, {'c', 3}, {'d', 2}, {'e', 1}};
//     using numsElemType = std::remove_all_extents<decltype(&nums[0])>::type;
//     m.insert<numsElemType>(&nums[0], &nums[5]);
//     EXPECT_EQ(5, m.size());
// }

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
